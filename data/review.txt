This restaurant is such an Italian dream come true! I came here for dinner with my boyfriend a few days, and we are still bragging about how phenomenal our experience was.

First off, it was the perfect weather, and they had their doors open to sit outside. The ambiance is stunning, very nature/green and positive vibes.

We both started off with the Old Cuban cocktail ($17). Expensive but worth it. It has a great kick but goes down smooth. And I love elderflower!

Our appetizers - OKAY GUYS GET READY. The Bruschetta Ricotta is the best thing I've ever eaten. Period. The ricotta is so fluffy and creamy and then they drizzle truffle oil on top. We were both saying wow after every bite. I've never had anything like this and I cannot wait to go back. Then, we had a burrata, and it was FANTASTIC.

For dinner, I had the margherita pizza with ricotta and my boyfriend has the mezzaluna. Both out of this world. Their pizza is doughy, warm, and soft. I loved it so much. My boyfriend had a clean plate very quickly.

The service was wonderful--our server was Alex. She was very knowledgeable about the menu, friendly, and helpful. All the food came out quickly and was timed well between courses.

Cannot wait to come back!!